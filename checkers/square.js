const createSquare = (color, coordinates, displayCoordinates, contents) => {
    const square = {
        color,
        coordinates,
        displayCoordinates,
        contents
    };

    return square;
}

module.exports = { createSquare }