const { data, filterIsDivisibleBy, successCallback } = require('./data-store')

/*
    Problem :   Show me the result of using the imported function filterIsDivisibleBy
                with imported data with divisibleNumber values from 1 to 10.  
                For Example, filterIsDivisibleBy(data, 1, <youcallbackhere>),
                filterIsDivisibleBy(data, 2, <youcallbackhere>) and so on up to 10.  I would
                like the results of each function call logged to the console.  

    Hint:       Console.log should be called 10 times and filterIsDivisibleBy should be called 10 times
    
*/
for (i = 1; i < 11; i++) {

    filterIsDivisibleBy(data, i, successCallback)

    //filterIsDivisibleBy(data, i, (divisibleNumber, result) =>
    //console.log(divisibleNumber, result)
    //) also a viable option, as opposed to cached 

}