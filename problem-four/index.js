/*
    Problem :   Given the array of items below, turn the corresponding 
                array into 1 paragraph of text.  Each line should should 
                follow the below specified format.  Given the array has 50 items,
                there should be 50 sentences in the paragraph.  Each 
                Sentence should begin on a new line.

    Format:     Hello, this is a sentence for the following: Id - <id>, Date - <date>, 
                totalArrayLength - <length>, originalText: <text>.
    
*/

const randomNumberGenerator = (max) => Math.floor(Math.random() * max);

const addDays = (date, days) => {
    date.setDate(date.getDate() + days);
}

const items = new Array(50).fill().map((w, i, arr) => {  

    const id = ++i;
    const date = new Date();
    const days = randomNumberGenerator(20);
    addDays(date, days);

    return {
        id,
        totalArrayLength: arr.length,
        originalText: `Some Message for Id: ${id}`,
        date
    }
})

let finalStr = "";

strFunction = () => {

    for(let i = 0; i < items.length; i++) {

        const idString = items[i]['id']
        const dateString = items [i]['date']
        const arrayLengthString = items[i]['totalArrayLength']
        const origString = items[i]['originalText']
    
        finalStr += `Hello, this is a sentence for the following: Id - ${idString}, Date - ${dateString},
         totalArrayLength: ${arrayLengthString}, originalText: ${origString}` + "\n"
    
    }
    
    return finalStr

}

strFunction()
console.log(finalStr)
