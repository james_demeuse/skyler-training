const createPiece = (color, coordinates) => {
    const { x, y } = coordinates;
    const piece = {
        color,
        coordinates: {
            x, 
            y
        }
    };

    return piece;
}

module.exports = { createPiece }