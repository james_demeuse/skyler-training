const randomNumberGenerator = (max) => Math.floor(Math.random() * max);

const isDivisibleBy = (value, divisibleNumber) => {
    return value % divisibleNumber === 0;
}

const getData = (onSuccess, onError) => {

    const divisibleNumber = randomNumberGenerator(10000);

    if (isDivisibleBy(divisibleNumber, 10) === true) {
        onSuccess(divisibleNumber)
        return;
    }

    onError('Number is not divisible by 10', divisibleNumber);

}

module.exports = { getData };

//Could we not utilize export const? Is this a way to export more than one function/variable?