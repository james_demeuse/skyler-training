const { createBoard, printBoard, movePiece, getLetterFromIndex } = require('./board');
const { createPiece } = require('./piece');
const { createSquare } = require('./square');
const readline = require("readline");

const setSquares = (x, y) => {

    const isEven = (x + y) % 2 == 0;
    const color = isEven ? 'white' : 'black';
    const square = createSquare(color, { x, y }, { x: getLetterFromIndex(x), y: (8 - y) }, null);

    if (isEven === false && y < 3) {
        square.contents = createPiece('black', { x, y });
    }

    if (isEven === false && y > 4) {
        square.contents = createPiece('white', { x, y });
    }

    return square;
}

const board = createBoard(setSquares)

console.log(printBoard(board));

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question("What is your name? ", function (answer) {

    // valid answer: b6,a5
    const [source, destination] = answer.split(",");
    movePiece(board, source, destination);
    console.log(printBoard(board));
    rl.close();
});

