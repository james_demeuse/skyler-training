/*
    Problem : Find me the largest number in this array and log it to the console
*/

const numbers = Array.from({length: 10}, (_, i) => i + 1)
//Using Math directly on  variable numbers will not work, as Math expects int values
//numbers will provide an array, not an int
const max = Math.max.apply(Math, numbers);
//we use apply to accept the array as int parameters
console.log(max)
