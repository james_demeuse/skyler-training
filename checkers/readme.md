Step 1:     Create a 8x8 2 dimensional array inside board.js.  The variable already exists for board, 
            but is assigned to nothing, please set your 2d array there.
Step 2:     Use createBoard in game.js to create a board.  Set that board equal to a variable called "board".  Also,
            use the callback in createBoard to setup the board properly.  There is a create piece function that
            should be used to create a piece.  

            NOTE:  Piece colors should be "black" and "red".  Only alter game.js, no other files can be altered

Step 3:     Print bottom display coordinates inside of "printBoard".  (a-h)
Step 4:     Create a function that takes in board and piece and computes valid movements,
            for the given piece.  The output of the function should be an array of valid movement
            coordinates