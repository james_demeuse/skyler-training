/*
    Problem :   Given the below snake case text, turn it into camel case and
                log the result to the console
    
*/

const text = "TAKE_THIS_LONG_SNAKE_CASE_TEXT_AND_TURN_IT_INTO_CAMEL_CASE";

const lowerCase = text.toLowerCase()

const strSplit = lowerCase.split('_')

let newString = strSplit[0]

const capitalizeLetter = (str) => {

    return str[0].toUpperCase() + str.slice(1)

}

for(let i = 1; i < strSplit.length; i++) {

    newString += capitalizeLetter(strSplit[i])

}

console.log(newString)

