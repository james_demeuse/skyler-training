const randomNumberGenerator = (max) => Math.floor(Math.random() * max);
const data = Array.from({ length: 100 }, () => randomNumberGenerator(10000));


const isDivisibleBy = (value, divisibleNumber) => {
    return value % divisibleNumber === 0;
}

const filterIsDivisibleBy = (array, divisibleNumber, sucessCallback) => {

    setTimeout(() => {
        const resultDisivible = array.filter(w => isDivisibleBy(w, divisibleNumber));

        sucessCallback(divisibleNumber, returnDivisible,);
    }, 1000)
}

const successCallback = (divisibleNumber, resultDisivible) => {

    console.log(divisibleNumber, resultDisivible)

}


module.exports = { data, filterIsDivisibleBy, successCallback};

//Could we not utilize export const? Is this a way to export more than one function/variable?