const {getData} = require('./data-store')

/*
    Problem :   We have one imported method called "getData", call this method 100 times.  During the 100 calls, use the callbacks 
                to put the successful numbers into one array, and put the unsuccessful numbers into another array.  Please log both of
                the successful and unsuccessful arrays.  

    Hint:       Console.log should be called 2 times, one for successful numbers, and one for unsuccessful numbers

    NOTE:       Do not alter data-store.js
    
*/
let successArray = []
let errorArray = []

for(let i=1; i<101; i++) {

    const onSuccess = (x) => {

        successArray.push(x)

    }

    const onError = (y, z) => {

        errorArray.push(z)

    }

    getData(onSuccess, onError)

}

console.log('Successful: ' + successArray)
console.log('Unsuccessful: ' + errorArray)
