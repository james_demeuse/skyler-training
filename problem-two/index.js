/*
    Problem :   Sort randomArray by the Id property from largest to smallest and
                log the result to the console
*/

const randomNumberGenerator = (max) => Math.floor(Math.random() * max);
const randomStringGenerator = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


const randomArray = Array.from({ length: 25 }, () => {
    return {
        Id: randomNumberGenerator(100),
        Value: randomStringGenerator(10)
    }
})

randomArray.sort((a,b) => b.Id - a.Id)

//Above, we compare array entries
//if result is negative, we say that A needs to come before B.

console.log(randomArray)