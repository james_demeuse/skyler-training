/*
    Problem :   Given randomArray below, return two new arrays, one with only numbers,
                one with only strings and log each to the console
*/

const randomNumberGenerator = (max) => Math.floor(Math.random() * max);
const randomStringGenerator = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const randomArray = Array.from({ length: 25 }, () => {
    return randomNumberGenerator(10) % 2 === 0 ?  randomNumberGenerator(100) : randomStringGenerator(10)
})

const intArray = randomArray.filter(num => Number.isInteger(num))

//filtering numerical inputs from randomArray

const strArray = randomArray.filter(num => typeof num === 'string')

//filtering string inputs from randomArray

console.log(intArray)
console.log(strArray)