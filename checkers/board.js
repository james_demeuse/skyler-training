const createBoard = (callback) => {
    const board = [];

    //made board an empty array

    for (let i = 0; i < 8; i++) {

        board[i] = [];

        //made each row an array

        for (let j = 0; j < 8; j++) {

            board[i][j] = callback(j, i);

            //populated each of the 8 arrays with 8 placeholders

        }
    }

    return board;
}

const movePiece = (board, source, destination) => {

    const sourceX = getIndexFromLetter(source[0]);
    const sourceY = getIndexFromNumber(source[1]);
    
    const destinationX = getIndexFromLetter(destination[0]);
    const destinationY = getIndexFromNumber(destination[1]);


    const piece = board[sourceY][sourceX].contents;
    board[sourceY][sourceX].contents = null;

    piece.coordinates.x = destinationX;
    piece.coordinates.y = destinationY;

    board[destinationY][destinationX].contents = piece
}

const printBoard = (board) => {
    let display = "";

    for (let i = 0; i < board.length; i++) {
        const row = board[i];

        if (i == 0) {

            for (let j = 0; j < row.length; j++) {
                const square = row[j];
                display += `\t${square.displayCoordinates.x}`;
            }
            display += `\r\n`;

        }

        display += `${row[0].displayCoordinates.y} [`;

        for (let j = 0; j < row.length; j++) {
            const square = row[j];

            display += square.contents == null ? "\t" : `\t${square.contents.color.slice(0, 1)}`;

            const isLastColumn = j + 1 === row.length;

            if (isLastColumn === true) {
                display += "\t";
            }
            
        }

        display += `] ${row[0].displayCoordinates.y}\r\n`;

        if (i == 7) {

            for (let j = 0; j < row.length; j++) {
                const square = row[j];
                display += `\t${square.displayCoordinates.x}`;
            }
            display += `\r\n`;

        }

        
    }

    return display;
}

const getLetterFromIndex = (index) => {
    const letters = 'abcdefgh';

    return letters[index];
}

const getIndexFromLetter = (letter) => {
    const letters = 'abcdefgh';

    for(let i = 0; i < letters.length; i++) {
        const char = letters[i];

        if (letter === char) {
            return i;
        }
    }

    return -1
}

const getIndexFromNumber = (number) => {
    return 8 - number;
}

module.exports = { createBoard, printBoard, movePiece, getLetterFromIndex, getIndexFromLetter, getIndexFromNumber }